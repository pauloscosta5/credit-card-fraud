"""Generate features for predicting transaction fraud."""
import pathlib
from typing import List

import numpy as np
import pandas as pd
import yaml
from pandas.tseries.holiday import USFederalHolidayCalendar


def write_feature(data: pd.DataFrame, feature_name: str) -> None:
    """
    Store the feature as a CSV file in the features directory.

    Will not record DataFrame index.

    Parameters
    ----------
    data: DataFrame
        Pandas DataFrame containing data. Ideally the DataFrame has two
        columns--one named "transaction_id" and one for the feature.
    feature_name : str or List of str
        Name of the feature(s). Will become the column name of the
        feature(s) as well as the filename.
    """
    data = data[["transaction_id", feature_name]]
    current_file_path = pathlib.Path(__file__).parent.resolve()
    feature_file_path = (
        current_file_path / f"data/features/{feature_name}.csv"
    ).resolve()
    data.to_csv(feature_file_path, index=False)


def read_ids(filename: str) -> List[int]:
    """
    Return a list of IDs sotred in YAML files.

    Parameters
    ----------
    filename : str
        Name of YAML file. File extension need not be specified.

    Returns
    -------
    IDs : List[int]
        A list of transaction IDs
    """
    filename = filename.replace(".yaml", "").replace(".yml", "")
    current_file_path = pathlib.Path(__file__).parent.resolve()
    id_file_path = (current_file_path / f"data/features/{filename}.yaml").resolve()
    with open(id_file_path, mode="r") as ids:
        return yaml.safe_load(ids)


def is_multi_swipe(data: pd.DataFrame) -> None:
    """
    Determine if transaction is involved in a group of multi-swipes.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id"]]
    multi_swipe_ids = read_ids("multi_swipe_ids")
    data["is_multi_swipe"] = data["transaction_id"].isin(multi_swipe_ids)
    write_feature(data, feature_name="is_multi_swipe")


def is_reversed(data: pd.DataFrame) -> None:
    """
    Determine if transaction is involved in a reversal.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id"]]
    multi_swipe_ids = read_ids("reversed_ids")
    data["is_reversed"] = data["transaction_id"].isin(multi_swipe_ids)
    write_feature(data, feature_name="is_reversed")


def is_correct_cvv(data: pd.DataFrame) -> None:
    """
    Determine if cardCVV is equal to enteredCVV.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "cardCVV", "enteredCVV"]]
    data["is_correct_cvv"] = data["cardCVV"] == data["enteredCVV"]
    write_feature(data, feature_name="is_correct_cvv")


def number_recent_account_transactions(data: pd.DataFrame) -> None:
    """
    Return the number of account transactions in the last eight hours.

    .. note::

        This implimentation takes too long on my laptop, and will
        therefore be excluded from the current model.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "accountNumber", "transactionDateTime"]]
    data = data.merge(data, how="inner", on="accountNumber", suffixes=("", "_prior"))
    data = data[
        data["transaction_id"] != data["transaction_id_prior"]
    ]  # Filter out cases where matches with self
    data["date_diff"] = data["transactionDateTime"] - data["transactionDateTime_prior"]
    data = data[
        (pd.Timedelta(0) < data["date_diff"])
        & (data["date_diff"] <= pd.Timedelta("8 hours"))
    ]
    data = (
        data.groupby("transaction_id")["date_diff"]
        .count()
        .reset_index(name="number_recent_account_transactions")
    )
    write_feature(data, feature_name="number_recent_account_transactions")


def seconds_since_account_transaction(data: pd.DataFrame) -> None:
    """
    Return seconds since the account was last involved in a transaction.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "accountNumber", "transactionDateTime"]]
    prior_transactions = pd.merge_asof(
        left=data,
        right=data,
        on="transactionDateTime",
        by="accountNumber",
        suffixes=("", "_prior"),
        direction="backward",
        allow_exact_matches=False,
    )
    prior_transactions = prior_transactions[
        prior_transactions["transaction_id"]
        != prior_transactions["transaction_id_prior"]
    ]  # Filter out cases where matches with self

    # Get transaction dates of prior transaction
    prior_transactions = prior_transactions.merge(
        data[["transaction_id", "transactionDateTime"]],
        left_on="transaction_id_prior",
        right_on="transaction_id",
        how="left",
        suffixes=("", "_prior"),
    )

    prior_transactions["seconds_since_account_transaction"] = (
        prior_transactions["transactionDateTime"]
        - prior_transactions["transactionDateTime_prior"]
    ).dt.seconds
    write_feature(prior_transactions, feature_name="seconds_since_account_transaction")


def transaction_day_of_week(data: pd.DataFrame) -> None:
    """
    Return the day of the week the transaction occurred.

    Day 0 is Monday.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionDateTime"]]
    data["transaction_day_of_week"] = data["transactionDateTime"].dt.dayofweek
    write_feature(data, feature_name="transaction_day_of_week")


def transaction_month(data: pd.DataFrame) -> None:
    """
    Return the month the transaction occurs.

    Month 1 is January.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionDateTime"]]
    data["transaction_month"] = data["transactionDateTime"].dt.month
    write_feature(data, feature_name="transaction_month")


def transaction_on_us_holiday(data: pd.DataFrame) -> None:
    """
    Determine if the transaction occurs on a US holiday.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionDateTime"]]
    us_holidays = USFederalHolidayCalendar().holidays(
        start="2016-01-01", end="2017-01-01"
    )
    data["transaction_on_us_holiday"] = (
        data["transactionDateTime"].dt.normalize().isin(us_holidays)
    )
    write_feature(data, feature_name="transaction_on_us_holiday")


def is_cross_country_transaction(data: pd.DataFrame) -> None:
    """
    Determine if acqCountry equals merchantCountryCode.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "acqCountry", "merchantCountryCode"]]
    data["is_cross_country_transaction"] = (
        data["acqCountry"] == data["merchantCountryCode"]
    )
    write_feature(data, feature_name="is_cross_country_transaction")


def seconds_since_address_change(data: pd.DataFrame) -> None:
    """
    Return seconds since address was changed.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[
        ["transaction_id", "dateOfLastAddressChange", "transactionDateTime"]
    ]
    data["seconds_since_address_change"] = (
        data["transactionDateTime"] - data["dateOfLastAddressChange"]
    ).dt.seconds
    write_feature(data, feature_name="seconds_since_address_change")


def is_special_merchantCategoryCode(data: pd.DataFrame) -> None:
    """
    Determine if transaction has a special merchantCategoryCode.

    Special merchantCategoryCode values are

    - fuel
    - mobileapps
    - online_subscriptions
    - food_delivery
    - gym
    - cable/phone

    These categories are seemed special because they are the only
    merchantCategoryCode values which no multi-swipes or reversed
    transactions.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "merchantCategoryCode"]]
    special_categories = [
        "fuel",
        "mobileapps",
        "online_subscriptions",
        "food_delivery",
        "gym",
        "cable/phone",
    ]
    data["is_special_merchantCategoryCode"] = data["merchantCategoryCode"].isin(
        special_categories
    )
    write_feature(data, feature_name="is_special_merchantCategoryCode")


def availableMoney_over_creditLimit(data: pd.DataFrame) -> None:
    """
    Divide availableMoney column by creditLimit.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "availableMoney", "creditLimit"]]
    data["availableMoney_over_creditLimit"] = (
        data["availableMoney"] / data["creditLimit"]
    )
    write_feature(data, feature_name="availableMoney_over_creditLimit")


def transactionAmount_over_availableMoney(data: pd.DataFrame) -> None:
    """
    Divide transactionAmount column by availableMoney.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionAmount", "availableMoney"]]
    data["transactionAmount_over_availableMoney"] = (
        data["transactionAmount"] / data["availableMoney"]
    )
    # Due to negative and 0 values for "availableMoney", some ratios can
    # be infinity or a negative value. Replace those values with 10%
    # over the maximum value
    maximum_ratio = data[data["transactionAmount_over_availableMoney"] != np.inf][
        "transactionAmount_over_availableMoney"
    ].max()
    data.loc[
        (data["transactionAmount_over_availableMoney"] == np.inf)
        | (data["transactionAmount_over_availableMoney"] < 0),
        "transactionAmount_over_availableMoney",
    ] = (maximum_ratio * 1.1)
    write_feature(data, feature_name="transactionAmount_over_availableMoney")


def transactionAmount_over_creditLimit(data: pd.DataFrame) -> None:
    """
    Divide transactionAmount column by creditLimit.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionAmount", "creditLimit"]]
    data["transactionAmount_over_creditLimit"] = (
        data["transactionAmount"] / data["creditLimit"]
    )
    write_feature(data, feature_name="transactionAmount_over_creditLimit")


def currentBalance_over_creditLimit(data: pd.DataFrame) -> None:
    """
    Divide currentBalance column by creditLimit.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "currentBalance", "creditLimit"]]
    data["currentBalance_over_creditLimit"] = (
        data["currentBalance"] / data["creditLimit"]
    )
    write_feature(data, feature_name="currentBalance_over_creditLimit")


def currentBalance_over_availableMoney(data: pd.DataFrame) -> None:
    """
    Divide transactionAmount column by availableMoney.

    .. note::

        Noticed at the last minute that this was implimented
        incorrectly. Should have been

        >>> data["currentBalance"] / data["availableMoney"]

        instead of

        >>> data["transactionAmount"] / data["availableMoney"]

        Given more time, this issue would have been fixed, and the model
        retrained on the new feature.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionAmount", "availableMoney"]]
    data["currentBalanceover_availableMoney"] = (
        data["transactionAmount"] / data["availableMoney"]
    )
    # Due to negative and 0 values for "availableMoney", some ratios can
    # be infinity or a negative value. Replace those values with 10%
    # over the maximum value
    maximum_ratio = data[data["currentBalanceover_availableMoney"] != np.inf][
        "currentBalanceover_availableMoney"
    ].max()
    data.loc[
        (data["currentBalanceover_availableMoney"] == np.inf)
        | (data["currentBalanceover_availableMoney"] < 0),
        "transactionAmount_over_availableMoney",
    ] = (maximum_ratio * 1.1)
    write_feature(data, feature_name="currentBalanceover_availableMoney")


def seconds_until_expiration(data: pd.DataFrame) -> None:
    """
    Return the seconds until the account expires.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionDateTime", "currentExpDate"]]
    data["seconds_until_expiration"] = (
        data["currentExpDate"] - data["transactionDateTime"]
    ).dt.seconds
    write_feature(data, feature_name="seconds_until_expiration")


def seconds_since_account_opened(data: pd.DataFrame) -> None:
    """
    Return the seconds since the account was opened.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "transactionDateTime", "accountOpenDate"]]
    data["seconds_since_account_opened"] = (
        data["accountOpenDate"] - data["transactionDateTime"]
    ).dt.seconds
    write_feature(data, feature_name="seconds_since_account_opened")


def is_first_account_transaction(data: pd.DataFrame) -> None:
    """
    Determine if transaction is the account's first.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[["transaction_id", "accountNumber", "transactionDateTime"]]
    first_transaction_date = (
        data.groupby("accountNumber")["transactionDateTime"]
        .min()
        .reset_index(name="first_account_transaction")
    )
    data = data.merge(first_transaction_date, on="accountNumber")
    data["is_first_account_transaction"] = (
        data["transactionDateTime"] == data["first_account_transaction"]
    )
    write_feature(data, feature_name="is_first_account_transaction")


def is_same_acqCountry_as_prior(data: pd.DataFrame) -> None:
    """
    Determine if acqCountry is equal to that of the account's previous transaction.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[
        ["transaction_id", "accountNumber", "acqCountry", "transactionDateTime"]
    ]
    data = pd.merge_asof(
        left=data,
        right=data,
        by="accountNumber",
        on="transactionDateTime",
        suffixes=("", "_prior"),
        direction="backward",
        allow_exact_matches=False,
    )
    data["is_same_acqCountry_as_prior"] = data["acqCountry"] == data["acqCountry_prior"]
    write_feature(data, feature_name="is_same_acqCountry_as_prior")


def is_same_merchantCountryCode_as_prior(data: pd.DataFrame) -> None:
    """
    Determine if merchantCountryCode is equal to the account's previous transaction.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    data = data.copy()[
        [
            "transaction_id",
            "accountNumber",
            "merchantCountryCode",
            "transactionDateTime",
        ]
    ]
    data = pd.merge_asof(
        left=data,
        right=data,
        by="accountNumber",
        on="transactionDateTime",
        suffixes=("", "_prior"),
        direction="backward",
        allow_exact_matches=False,
    )
    data["is_same_merchantCountryCode_as_prior"] = (
        data["merchantCountryCode"] == data["merchantCountryCode_prior"]
    )
    write_feature(data, feature_name="is_same_merchantCountryCode_as_prior")


def create_features(data: pd.DataFrame) -> None:
    """
    Run all feature functions.

    Parameters
    ----------
    data : pd.DataFrame
        Pandas DataFrame containing all data for modeling.
    """
    features = (
        is_multi_swipe,
        is_reversed,
        is_correct_cvv,
        # number_recent_account_transactions,  Taking too long to run
        seconds_since_account_transaction,
        transaction_day_of_week,
        transaction_month,
        transaction_on_us_holiday,
        is_cross_country_transaction,
        seconds_since_address_change,
        is_special_merchantCategoryCode,
        availableMoney_over_creditLimit,
        transactionAmount_over_availableMoney,
        transactionAmount_over_creditLimit,
        currentBalance_over_creditLimit,
        currentBalance_over_availableMoney,
        seconds_until_expiration,
        seconds_since_account_opened,
        is_first_account_transaction,
        is_same_acqCountry_as_prior,
        is_same_merchantCountryCode_as_prior,
    )
    for feature in features:
        feature(data)
