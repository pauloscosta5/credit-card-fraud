# Credit card transactions

_A project analyzing credit card transaction data and creating a model predicting fraud._

## Read report and findings

The full findings may be easily viewed in [`transactions.ipynb`](transactions.ipynb). An accompanying high level report may be found in [`reports/report.md`](reports/report.md).

## How to run

The code may also be ran by creating the appropriate environment and opening. `transactions.ipynb`.

Assuming you have conda installed a local environment with [Jupyter Notebook](https://jupyter.org/install) and [`nb_conda_kernels`](https://github.com/Anaconda-Platform/nb_conda_kernels), simply run

```zsh
conda create -f environment.yaml
```

Now run the notebook by running the following command.

```zsh
jupyter notebook
```

and open the file `transactions.ipynb`. From the menu bar, select Kernel → Change kernel → `Python[conda env:credit_env]`.

You should now be able to run the code in a Jupyter Notebook.

Alternatively, the report is also available in `transactions.py`.

## Files

Quick description of all files in the directory.

- [`reports/transactions.html`](reports/report.md) HTML version of `transactions.ipynb`. Useful for quick viewing.
- [`reports/report.md`](reports/report.md) High level report of findings detailed in `transactions.html`
- `transactions.ipynb` – Main file containing report and accompanying code
- `features.py` – Python file containing code and descriptions for each feature fed into the model presented. Imported into `transactions.ipynb`
- `environment.yaml` – File detailing the Python environment used to write the presented code. Replicate the environment by running `conda create -f environment.yaml`
- `model_eval_plots.py` – Functions to plot model performance that are imported into `transactions.ipynb`
- `transactions.py` – Python scrip file synced to `transactions.ipynb` via Jupytext. Contains the same content as the notebook.
- `material_theme_light.mplstyle` – Theme file for plot shown in `transactions.ipynb`
- `.flake8`, `.isort.cfg`, `mypy.ini` – Config files for linting Python code
- `data/cleaned` – The directory where the cleaned data was stored
- `data/raw` – The directory where the original data was stored
- `data/features` – The directory where the feature data was stored
