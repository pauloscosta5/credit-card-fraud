# Report

_A summary of the findings detailed in `transactions.ipynb`._

## Loading the data

### Downloading the data

Data is loaded into a Pandas DataFrame. Four columns—`transactionDateTime`, `currentExpDate`, `accountOpenDate`, and
`dateOfLastAddressChange`— are identified as dates or times and are converted accordingly. Blank strings (`""`) are
found in multiple columns such as `merchantState` and `merchantZip`. Those blank strings are treated as missing
values and are replaced with `NaN`.

### Data structure

The data contains 641,914 records, each containing 29 fields—six of which are empty.

### Summary statistics

The minimum and maximum values—along with other accompanying summary statistics—are provided for date and numerical columns that are not ids. Unique values are provided for categorical data where the number of unique values is found to be less than 100.

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>creditLimit</th>
      <th>availableMoney</th>
      <th>transactionAmount</th>
      <th>currentBalance</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>641914.000000</td>
      <td>641914.000000</td>
      <td>641914.000000</td>
      <td>641914.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>10697.210608</td>
      <td>6652.828573</td>
      <td>135.162497</td>
      <td>4044.382035</td>
    </tr>
    <tr>
      <th>std</th>
      <td>11460.359133</td>
      <td>9227.132275</td>
      <td>147.053302</td>
      <td>5945.510224</td>
    </tr>
    <tr>
      <th>min</th>
      <td>250.000000</td>
      <td>-1244.930000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>5000.000000</td>
      <td>1114.970000</td>
      <td>32.320000</td>
      <td>502.442500</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>7500.000000</td>
      <td>3578.165000</td>
      <td>85.800000</td>
      <td>2151.860000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>15000.000000</td>
      <td>8169.185000</td>
      <td>189.030000</td>
      <td>5005.890000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>50000.000000</td>
      <td>50000.000000</td>
      <td>1825.250000</td>
      <td>47496.500000</td>
    </tr>
  </tbody>
</table>
</div>

### Date values

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>transactionDateTime</th>
      <th>currentExpDate</th>
      <th>accountOpenDate</th>
      <th>dateOfLastAddressChange</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>641914</td>
      <td>641914</td>
      <td>641914</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>unique</th>
      <td>635472</td>
      <td>165</td>
      <td>1826</td>
      <td>2186</td>
    </tr>
    <tr>
      <th>top</th>
      <td>2016-01-11 00:34:57</td>
      <td>2026-05-01 00:00:00</td>
      <td>2015-12-11 00:00:00</td>
      <td>2016-07-20 00:00:00</td>
    </tr>
    <tr>
      <th>freq</th>
      <td>3</td>
      <td>4209</td>
      <td>10137</td>
      <td>3948</td>
    </tr>
    <tr>
      <th>first</th>
      <td>2016-01-01 00:00:37</td>
      <td>2018-05-01 00:00:00</td>
      <td>1985-12-25 00:00:00</td>
      <td>1985-12-25 00:00:00</td>
    </tr>
    <tr>
      <th>last</th>
      <td>2016-12-30 23:59:27</td>
      <td>2032-01-01 00:00:00</td>
      <td>2015-12-31 00:00:00</td>
      <td>2016-12-30 00:00:00</td>
    </tr>
  </tbody>
</table>
</div>

### Missing value counts

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>column_name</th>
      <th>number_of_nulls</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>posOnPremises</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>1</th>
      <td>merchantZip</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>2</th>
      <td>merchantState</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>3</th>
      <td>merchantCity</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>4</th>
      <td>echoBuffer</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>5</th>
      <td>recurringAuthInd</td>
      <td>641914</td>
    </tr>
    <tr>
      <th>6</th>
      <td>acqCountry</td>
      <td>3913</td>
    </tr>
    <tr>
      <th>7</th>
      <td>posEntryMode</td>
      <td>3345</td>
    </tr>
    <tr>
      <th>8</th>
      <td>merchantCountryCode</td>
      <td>624</td>
    </tr>
    <tr>
      <th>9</th>
      <td>transactionType</td>
      <td>589</td>
    </tr>
    <tr>
      <th>10</th>
      <td>posConditionCode</td>
      <td>287</td>
    </tr>
    <tr>
      <th>11</th>
      <td>merchantName</td>
      <td>0</td>
    </tr>
    <tr>
      <th>12</th>
      <td>expirationDateKeyInMatch</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>transactionAmount</td>
      <td>0</td>
    </tr>
    <tr>
      <th>14</th>
      <td>transactionDateTime</td>
      <td>0</td>
    </tr>
    <tr>
      <th>15</th>
      <td>creditLimit</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16</th>
      <td>customerId</td>
      <td>0</td>
    </tr>
    <tr>
      <th>17</th>
      <td>availableMoney</td>
      <td>0</td>
    </tr>
    <tr>
      <th>18</th>
      <td>dateOfLastAddressChange</td>
      <td>0</td>
    </tr>
    <tr>
      <th>19</th>
      <td>merchantCategoryCode</td>
      <td>0</td>
    </tr>
    <tr>
      <th>20</th>
      <td>currentExpDate</td>
      <td>0</td>
    </tr>
    <tr>
      <th>21</th>
      <td>accountOpenDate</td>
      <td>0</td>
    </tr>
    <tr>
      <th>22</th>
      <td>cardCVV</td>
      <td>0</td>
    </tr>
    <tr>
      <th>23</th>
      <td>enteredCVV</td>
      <td>0</td>
    </tr>
    <tr>
      <th>24</th>
      <td>cardLast4Digits</td>
      <td>0</td>
    </tr>
    <tr>
      <th>25</th>
      <td>isFraud</td>
      <td>0</td>
    </tr>
    <tr>
      <th>26</th>
      <td>currentBalance</td>
      <td>0</td>
    </tr>
    <tr>
      <th>27</th>
      <td>cardPresent</td>
      <td>0</td>
    </tr>
    <tr>
      <th>28</th>
      <td>accountNumber</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>

#### Unique values

    isFraud
    -----





    array([ True, False])





    cardPresent
    -----





    array([False,  True])





    expirationDateKeyInMatch
    -----





    array([False,  True])





    acqCountry
    ---------





    array(['US', nan, 'PR', 'MEX', 'CAN'], dtype=object)





    merchantCountryCode
    ---------





    array(['US', 'PR', 'MEX', nan, 'CAN'], dtype=object)





    posEntryMode
    ---------





    array(['05', '09', '02', '90', nan, '80'], dtype=object)





    posConditionCode
    ---------





    array(['01', '08', '99', nan], dtype=object)





    merchantCategoryCode
    ---------





    array(['rideshare', 'online_gifts', 'personal care', 'fastfood',
           'entertainment', 'online_subscriptions', 'mobileapps', 'fuel',
           'food', 'online_retail', 'airline', 'hotels', 'food_delivery',
           'cable/phone', 'subscriptions', 'auto', 'gym', 'furniture',
           'health'], dtype=object)





    transactionType
    ---------





    array(['PURCHASE', 'ADDRESS_VERIFICATION', 'REVERSAL', nan], dtype=object)





    creditLimit
    ---------





    array([ 5000,  7500,   250, 20000,   500, 50000,  2500, 15000,  1000,
           10000])

### Definitions of columns

`posEntryMode` – A code that identifies the method used to capture the account number and expiration date. [Reference table](https://dnwebdomestic2.efunds.com/dnweb/webhelp/Field_Descriptions/MC_POS_Entry_Mode.htm)

`posConditionCode` – Indicates transaction conditions at the point of sale or service.

`acqCountry` – Acquirer country?

`creditLimit` – Should be the sum of `availableMoney` and `currentBalance`

## Plotting data

### Transaction amounts

![png](output_27_0.png)

### Data structure

The distribution is highly skewed right. The first bin is the largest, with over 5% of transactions being under \$3.64. As expected, a large majority of transactions are lower cost. High transaction amount could be one indicator of potential fraudulent activity.

## Data cleaning: duplicate transactions

### Identifying duplicate transactions

First, IDs are assigned to each transaction to uniquely identify them. The following columns are then used to identify transactions to match them up with their duplicates

```python
[
    "accountNumber",
    "customerId",
    "creditLimit",
    "transactionAmount",
    "merchantName",
    "acqCountry",
    "merchantCountryCode",
    "posEntryMode",
    "posConditionCode",
    "merchantCategoryCode",
    "accountOpenDate",
    "cardCVV",
    "enteredCVV",
    "cardLast4Digits",
]
```

The "merchantName" column has some values that contain a code following the actual name—such as "AMC #606218". I removed the code and left the name.

### Multi-swipe

Transactions that occur within 12 hours within each other and have identical values for all the above listed columns are flagged as multi-swipe.

### Reversed

Similarly, transactions that had the "transactionType" of "REVERSAL" were matched with transactions that were non-reversal based on the above columns.

### Findings

As seen in the two following plots, it can be seen that neither multi-swipe or
reversed transactions occur in a few `merchantCategoryCode` values—`fuel`,
`mobileapps`, `online_subscriptions`, `food_delivery`, `gym`, and `cable/phone`.

![png](output_47_0.png)

![png](output_48_0.png)

Estimated shares are displayed in the table below.

It is natural to assume that many multi-swipes are eventually reversed. The code above shows that there is some overlap between reversal transactions and multi-swipe transactions. Therefore, we will count the total number of transactions and balances for the combined totals separately—as to not double count.

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>total_balance</th>
      <th>number_of_transactions</th>
      <th>total_balance_proportion</th>
      <th>number_of_transactions_proportion</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>multi_swipes</th>
      <td>888131.91</td>
      <td>6190</td>
      <td>0.010508</td>
      <td>0.009892</td>
    </tr>
    <tr>
      <th>reversals</th>
      <td>2121434.38</td>
      <td>14604</td>
      <td>0.025100</td>
      <td>0.023338</td>
    </tr>
    <tr>
      <th>combined</th>
      <td>2993252.39</td>
      <td>20667</td>
      <td>0.035415</td>
      <td>0.033027</td>
    </tr>
  </tbody>
</table>
</div>

## Modeling fraud

One assumption I will be making is that fraud may be caught retroactively. I will be including features that indicate if the transaction was eventually reversed or if it is a multi-swipe. Both—especially the reversal—may happen some time after the initial charge occurs.

### Clean data

In order to remove duplicate transactions, I will be excluding all reversal transactions and removing the multi-swipe transactions identified earlier. In both cases I will be retaining the original transactions.

### Engineered features

Engineered features are listed as individual functions in the file [`features.py`](features.py). The function `create_features()` runs all the feature functions and outputs them as CSV files.

The features engineered are:

- Is the transaction a multi-swipe?
- Is the transaction reversed?
- Was the "cardCVV" equal to the "enteredCVV"?
- Seconds since account was last involved in a transaction
- Day of the week the transaction occurred
- Month the transaction occurs
- If the transaction occurs on a US holiday
- If acqCountry equals merchantCountryCode
- Seconds since address was changed
- If transaction has a "special" merchantCategoryCode
      - Special categories are: `fuel`, `mobileapps`, `online_subscriptions`, `food_delivery`, `gym`, and `cable/phone`
- availableMoney divided by creditLimit
- transactionAmount column divided by availableMoney
- transactionAmount column divided by creditLimit
- currentBalance column divided by creditLimit
- transactionAmount column divided by availableMoney
- Seconds until expiration
- Seconds until account opens
- If transaction is the account's first
- If acqCountry is equal to that of the account's previous transaction
- If merchantCountryCode is equal to the account's previous transaction.

### Missing values/pipeline

Of the remaining columns, five have missing values (not counting "seconds_since_account_transaction"—a generated feature).

The following are my strategies for dealing with the missing values per column:

- **merchantCountryCode** is already overwhelmingly "US", so missing values will be filled in with "US".
- **acqCountry** is found to usually be the same as the country found in "merchantCountryCode". If
  **merchantCountryCode** is available, use it. The class `AcqCountryImputer` was written to handle this case.
- **transactionType** is highly correlated to "transactionAmount". For all recorded cases, if the "transactionType" is "ADDRESS_VERIFICATION", then the "transactionAmount" is found to be 0.00. So if the "transactionAmount" value is 0.00, then it is assumed the "transactionType" to be "ADDRESS_VERIFICATION". If not, it will be set to "PURCHASE". The class `TransactionTypeImputer` was written to handle this case.
- **posEntryMode** and **posConditionCode** will each be filled with their most popular codes

Once all missing values are filled, categorical values are one hot encoded.

### Train model

### The model

The target value—"isFraud"—is very unbalanced, only 1.8% of all transactions were found to be fraud. To accommodate for this, the fraud cases were randomly oversampled to increase their representation in the training data.

80% of the total data was selected as a train set.

Gradient boosted trees were selected as the model—where trees are sequentially added to an ensemble, each one correcting its predecessor. Gradient boosted trees were chosen due primarily to the short timeframe of the project. Gradient boosted trees are a good all-around model that do not require scaling of features and can employ early stopping for easy hyperparameter tuning. In particular, the _XGBoost_ implementation of gradient boosted trees was used primarily for its training performance. _XGBoost_ can parallelize tree construction, has early stopping built in, and can bin continuous features.

The average precision was used to evaluate the model and tune the number of trees. It was chosen because it is a metric that summarizes the precision-recall curve and is fairly robust to imbalanced classes—as is the case in this dataset.

While not shown, isolation forests were also fit to the data—as they typically are proficient at outlier detection. However, at first glance this model seemed to underperform in comparison gradient boosted trees, so it was cast aside in favor of the current implementation.

Best score found throug early stopping is 581 trees (map of 0.091).

### Model performance

The model was evaluated using 3-fold cross validation. The mean of all average precision scores was found to be `0.084` . For context, the baseline average precision of a random classifier for this dataset is `0.018`. The model is a 467% improvement over a random classifier.

Accompanying plots are included to illustrate the model's performance.

The precision-recall curve is related to the average precision—the metric the model aimed to maximize. The curve—along with the accompanying ROC curve—illustrate the tradeoffs of different decision threshold values of the model.

![png](output_96_0.png)

![png](output_97_0.png)

The prediction probability distribution plot shows kernel density estimation of the model's predicted probabilities for transactions and colors them according to whether the transaction was fraudulent. Encouragingly, there is a clear distinction in the distribution of the predicted probabilities for the two classes, which means the model is effectively picking out signals that imply a fraudulent transaction. However, the two distributions have a large overlap, which means there is room to improve to help the model further separate the two classes. Interestingly, there is a sharp peak on the far left, indicating a group of transactions that the model is very sure are not fraudulent. It is worth clarifying that the areas under both curves are normalized to be equal, so even though fraudulent transactions are a fraction of the number of legitimate transactions, both peaks stand at about equal height.

The accompanying confusion matrix highlights that the model has a high amount of false positives, and could use some further work to help improve its precision.

![png](output_99_0.png)

![png](output_100_0.png)

### Model analysis

To provide some intuition for the model, and inform what next steps should be taken to further improve its performance, Shapley values are calculated for the predictions.

The Shapley values reveal that `is_special_merchantCategoryCode` is highly influential feature. It was an engineered feature that indicated whether the transaction involved a fuel, mobileapps, online_subscription, food_delivery, gym, or cable/phone merchant. These categories were deemed unique because no multi-swipe or reversed transactions were detected in those transactions.

Additionally, it can be seen that the data lacks strong indicators of a fraudulent transaction. Only five of the top 20 features are shown to contribute positive Shapley values (meaning they push the prediction towards identifying the transaction as fraudulent). The `transactionAmount` was the biggest contributor—with high values making it more likely that the transaction is fraudulent.

An accompanying plot looks at the Shapley values just for `transactionAmount` reveals an interesting relationship with the feature `creditLimit`. When the `transactionAmount` is low the `creditLimit` being high will mean a lower chance of the model predicting the transaction to be fraudulent, while if the `transactionAmount` is high, a high `creditLimit` will result in the model predicting a higher chance of predicting fraud. The feature `creditLimit` interacts similarly with other features—as shown in other plots

The final feature to be highlighted is the Shapley values for the false negatives—the fraud the model missed out on. It can be seen that the `transactionAmount` for these false negatives is typically low. The model relies heavily on higher `transactionAmount` values for fraud detection, and needs improvement when dealing with lower transaction amounts.

### All values

![png](output_104_0.png)

![png](output_105_0.png)

![png](output_106_0.png)

![png](output_107_0.png)

### False negatives

![png](output_109_0.png)

![png](output_110_0.png)

### Future work

The model is off to a great start, but needs some improvement in distinguishing fraudulent transactions.

One obvious step I omitted is grid search to optimize hyperparameters. Similarly I would look at other models such as logistic regression, and other anomaly detection models such as Gaussian mixture models and compare their performance to the current gradient boosted tree implementation.

However, much more importantly, big gains in model performance are gained through data cleaning and feature engineering. Given the time and more computing power, I would engineer more features. Some of the features I would like to engineer are:

- Number of transactions in the last hour/day/week/all-time for account/customer
- Total transaction amount in the last hour/day/week/all-time for account/customer
- Most popular merchant category/name of account/customer
  - Does the transaction involve the most popular merchant category/name?
- Is the transaction at a new merchant category/name for the account/customer?
- Has the customer/account been a victim of fraud before?
  - Total amount of fraud incidents for account/customer
- Average number of transactions per day/week for customer/account
- Average transaction amount per day/week for customer/account
- Holidays for different countries (only used US federal holidays)

When filling out the missing values for the columns "posEntryMode" and "posConditionCode" I simply filled them in with their most popular values. This is rarely the optimal method. If given more time I would search the data for a relationship with other values as done with other columns, find the most popular values for cohorts of transactions (the most popular "posEntryMode" per "transactionAmount" bin, etc.), or use a k-nearest neighbors model to more intelligently fill in missing values. I had lookup up some basic definitions for the two columns—"posEntryMode" identifies the method used to capture the account number, and "posConditionCode" identifies transaction conditions—but confidently knowing exactly what each code stood for would help in both filling values, feature engineering, and interpreting model Shapley values.

Additionally, when identifying multi-swipe and reversed transactions, I semi-arbitrarily chose 12 hours as the time window for seeking out these duplicate transactions. I know this led to some possible duplicate transactions being cut out. I would like to consult with a subject matter expect to tune this window appropriately.
