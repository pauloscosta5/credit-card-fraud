# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python [conda env:credit_env]
#     language: python
#     name: conda-env-credit_env-py
# ---

# %% [markdown] {"Collapsed": "false"}
# Instructions to run the notebook may be found in `README.md`.

# %% [markdown] {"Collapsed": "false"}
# # Imports

# %% {"Collapsed": "false"}
# Magics
# %matplotlib inline
# %config InlineBackend.figure_format = "retina"  # Higher resolution plots

# %% {"Collapsed": "false"}
# Imports
import os
import pathlib
import warnings
from typing import Optional, Union

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import shap
import yaml
from imblearn.over_sampling import RandomOverSampler
from imblearn.pipeline import Pipeline as ImPipeline
from IPython.core.interactiveshell import InteractiveShell
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.compose import ColumnTransformer
from sklearn.impute import MissingIndicator, SimpleImputer
from sklearn.model_selection import cross_val_predict, cross_val_score, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from xgboost.sklearn import XGBClassifier

import features
import model_eval_plots

# %% {"Collapsed": "false"}
# Configs
InteractiveShell.ast_node_interactivity = "all"  # Multiple outputs allowed
plt.style.use("material_theme_light.mplstyle")
shap.initjs()

# %% [markdown] {"Collapsed": "false"}
# ## Loading data

# %% [markdown] {"Collapsed": "false"}
# Data is loaded into a Pandas DataFrame. Four columns—`transactionDateTime`, `currentExpDate`, `accountOpenDate`, and
# `dateOfLastAddressChange`— are identified as dates or times and are converted accordingly. Blank strings (`""`) are
# found in multiple columns such as `merchantState` and `merchantZip`. Those blank strings are treated as missing
# values and are replaced with `NaN`.

# %% {"Collapsed": "false"}
# Peek at data
# !head -2 data/raw/transactions.txt

# %% {"Collapsed": "false"}
# Look for date columns
# !head -1 data/raw/transactions.txt | grep -i "date"

# %% {"Collapsed": "false"}
# Read data in while converting all columns identified in the previous
# cell to dates (besides expirationDateKeyInMatch, which is a bool)
transactions = pd.read_json(
    pathlib.Path("data/raw/transactions.txt"),
    orient="records",
    lines=True,
    convert_dates=[
        "transactionDateTime",
        "currentExpDate",
        "accountOpenDate",
        "dateOfLastAddressChange",
    ],
)

# %% {"Collapsed": "false"}
# Label blank strings as missing values
transactions = transactions.replace("", np.nan)

# %% {"Collapsed": "false"}
# Peek at loaded data
transactions.head()

# %% [markdown] {"Collapsed": "false"}
# The data contains 641,914 records, each containing 29 fields—six of which are empty.

# %% {"Collapsed": "false"}
# Number of records and fields
transactions.shape

# %% {"Collapsed": "false"}
# Get number of empty columns
(transactions.count() == 0).sum()

# %% [markdown] {"Collapsed": "false"}
# The minimum and maximum values—along with other accompanying summary statistics—are provided for date and numerical columns that are not ids. Unique values are provided for categorical data where the number of unique values is found to be less than 100.

# %% {"Collapsed": "false"}
# Get all empty columns to ignore
empty_columns = transactions.columns[transactions.count() == 0].tolist()

# Numerical columns that are categorical
categorical_num_columns = [
    "accountNumber",
    "customerId",
    "cardLast4Digits",
    "cardCVV",
    "enteredCVV",
]

# Get all numerical columns, excluding ids and empty columns
numerical_columns = (
    transactions.select_dtypes(include=[int, float])
    .drop(columns=categorical_num_columns + empty_columns)
    .columns
)

# %% {"Collapsed": "false"}
# Minimum and maximum for numerical columns
transactions[numerical_columns].describe()

# %% {"Collapsed": "false"}
# Min and maximum for datetime columns
transactions.describe(include=["datetime"])

# %% {"Collapsed": "false"}
# Count of null values per column
transactions.isna().sum().sort_values(ascending=False).reset_index(
    name="number_of_nulls"
).rename(columns={"index": "column_name"})

# %% {"Collapsed": "false"}
# Number of unique values

# For boolean columns
for column in transactions.select_dtypes(bool).columns:
    print("\n")
    print(column)
    print("-----")
    transactions[column].unique()

# For string columns, if number of unique values is less than 100
for column in transactions.select_dtypes(object).columns:
    unique_vals = transactions[column].unique()
    if unique_vals.shape[0] < 100:
        print("\n")
        print(column)
        print("---------")
        unique_vals

# For specific numerical columns
for column in ("creditLimit",):
    print("\n")
    print(column)
    print("---------")
    transactions[column].unique()

# %% [markdown] {"Collapsed": "false"}
# ### Definitions of columns
#
# `posEntryMode` – A code that identifies the method used to capture the account number and expiration date. [Reference table](https://dnwebdomestic2.efunds.com/dnweb/webhelp/Field_Descriptions/MC_POS_Entry_Mode.htm)
#
# `posConditionCode` – Indicates transaction conditions at the point of sale or service.
#
# `acqCountry` – Acquirer country?
#
# `creditLimit` – Should be the sum of `availableMoney` and `currentBalance`

# %% [markdown] {"Collapsed": "false"}
# ## Plotting transaction amounts

# %% [markdown] {"Collapsed": "false"}
# A histogram of the processed amounts of each transaction, the `transactionAmount` column.

# %% {"Collapsed": "false"}
ax = transactions["transactionAmount"].hist(bins=502)
ax.set_title("Number of transactions", loc="left")
ax.set_xlabel("Transaction amount")
ax.xaxis.grid(False)
ax.xaxis.set_major_formatter(mpl.ticker.StrMethodFormatter("${x:,.0f}"))
ax.yaxis.set_major_formatter(mpl.ticker.StrMethodFormatter("{x:,.0f}"))
ax.tick_params(axis="x", which="major", bottom=True);

# %% [markdown] {"Collapsed": "false"}
# The distribution is highly skewed right. The first bin is the largest, with over 5% of transactions being under $3.64. As expected, a large majority of transactions are lower cost. High transaction amount could be one indicator of potential fraudulent activity.

# %% {"Collapsed": "false"}
# Peek at amount in each bin
pd.cut(transactions["transactionAmount"], bins=502).value_counts(
    sort=False
).reset_index(name="count").rename(columns={"index": "bin"})

# %% {"Collapsed": "false"}
# Find the 80th percentile amount
transactions["transactionAmount"].quantile(0.80)

# %% [markdown] {"Collapsed": "false"}
# ## Identifying duplicate transactions

# %% [markdown] {"Collapsed": "false"}
# Transactions are given IDs. Two arrays below—`multi_swipe_transaction_ids` and `reversal_transaction_ids`—identify multi-swip and reversal transactions, respectively.

# %% [markdown] {"Collapsed": "false"}
# I am assuming that the columns listed in `transaction_id_columns` below may be used to uniquely identify a transaction. These columns will be used to pair up duplicate transactions to identify them as either multi-swipe or reversed transactions.

# %% {"Collapsed": "false"}
# Columns chosen to identify the transaction
transaction_id_columns = [
    "accountNumber",
    "customerId",
    "creditLimit",
    "transactionAmount",
    "merchantName",
    "acqCountry",
    "merchantCountryCode",
    "posEntryMode",
    "posConditionCode",
    "merchantCategoryCode",
    "accountOpenDate",
    "cardCVV",
    "enteredCVV",
    "cardLast4Digits",
]

# Sort date columns for use in ``merge_asof``
transactions = transactions.sort_values("transactionDateTime")

# Create an id to identify each transaction from the index
transactions["transaction_id"] = transactions.reset_index()["index"]

# %% [markdown] {"Collapsed": "false"}
# The "merchantName" column has some values that contain a code following the actual name—such as "AMC #606218". I removed the code and left the name.

# %% {"Collapsed": "false"}
transactions[transactions["merchantName"].str.contains("#")]["merchantName"].head(10)

# %% {"Collapsed": "false"}
# Remove trailing codes from merchantName
transactions["merchantName"] = transactions["merchantName"].str.split(" #").str[0]

# %% [markdown] {"Collapsed": "false"}
# ### Multi-swipe

# %% {"Collapsed": "false"}
# For all transactions, find if another transactions exists in the table
# that has all the same values for the columns specified in
# ``transaction_id_columns``--but occurs within 12 hours after the
# original transaction date
similar_transactions = pd.merge_asof(
    left=transactions,
    right=transactions,
    on="transactionDateTime",
    by=transaction_id_columns + ["transactionType"],
    tolerance=pd.Timedelta("12 hours"),
    allow_exact_matches=False,
    direction="forward",
    suffixes=["_before", "_after"],
)

# Remove transactions that get no match and grab the ids of the
# multi-swipe transactions
similar_transactions = similar_transactions[
    ~similar_transactions["transaction_id_after"].isna()
]
multi_swipe_transaction_ids = similar_transactions["transaction_id_after"]

# Count number of multi-swipes and sum up their balances
total_multi_swipes = similar_transactions.shape[0]
total_multi_swipe_balance = similar_transactions["transactionAmount"].sum()

# Remove transactions flagged as multi-swipes
non_multi_swipe_transactions = transactions[
    ~transactions["transaction_id"].isin(multi_swipe_transaction_ids)
]

# Store transactions that were multi-swiped for later use
with open(pathlib.Path("data/features/multi_swipe_ids.yaml"), mode="w") as file:
    file.write(yaml.safe_dump(similar_transactions["transaction_id_before"].tolist()))

# %% {"Collapsed": "false"}
similar_transactions.shape

# %% [markdown] {"Collapsed": "false"}
# The above code purposefully ignores cases where the date is an exact match.
# Manually check for similar transactions with the exact same `transactionDateTime`.

# %% {"Collapsed": "false"}
# Find identical transactions that occur at the exact same time
identical_transactions = non_multi_swipe_transactions.merge(
    non_multi_swipe_transactions,
    on=transaction_id_columns + ["transactionType", "transactionDateTime"],
    suffixes=["_before", "_after"],
)

# Return the number of identical tranactions with identical times
# Filter out cases where transaction matches with itself
identical_transactions[
    identical_transactions["transaction_id_after"]
    != identical_transactions["transaction_id_before"]
].shape[0]

# %% [markdown] {"Collapsed": "false"}
# ### Reversed

# %% {"Collapsed": "false"}
# Create 2 DataFrames--one without reversals, and one that is exclusively
# reversals
non_reversal_transactions = transactions[transactions["transactionType"] != "REVERSAL"]
reversal_transactions = transactions[transactions["transactionType"] == "REVERSAL"]

# Pair up reverals with their most similar transactions
# For each reversal, match it a similar non-reversal transactions and
# choose the transaction that happened most recently
paired_reversal_transactions = pd.merge_asof(
    left=reversal_transactions,
    right=non_reversal_transactions,
    on="transactionDateTime",
    by=transaction_id_columns,
    allow_exact_matches=True,
    direction="backward",
    suffixes=["_reversal", "_actual"],
)

# Remove transactions that did not find a match
# Grab the ids of transactions that have been reversed
paired_reversal_transactions = paired_reversal_transactions[
    ~paired_reversal_transactions["transaction_id_actual"].isna()
]
reversal_transaction_ids = paired_reversal_transactions["transaction_id_actual"]

# Count number of reversed transactions and sum up their balances
total_reversals = paired_reversal_transactions.shape[0]
total_reversal_balance = paired_reversal_transactions["transactionAmount"].sum()

# Store transactions that were reversed for later use
with open(pathlib.Path("data/features/reversed_ids.yaml"), mode="w") as file:
    file.write(yaml.safe_dump(reversal_transaction_ids.tolist()))


# %% [markdown] {"Collapsed": "false"}
# As seen in the two following plots, it can be seen that neither multi-swipe or
# reversed transactions occur in a few `merchantCategoryCode` values—`fuel`,
# `mobileapps`, `online_subscriptions`, `food_delivery`, `gym`, and `cable/phone`.

# %% {"Collapsed": "false"}
# Define function to plot out distribution differences between
# multi-swipe, reversal, and all transactions
def plot_comparision_bar(
    subject_ids: np.array,
    category_name: str,
    id_name: str,
    plot_title: str = "",
    limit: Optional[int] = None,
) -> mpl.axes._axes:
    """
    Compare a category distribution for the given IDs to that of all transactions.

    Parameters
    ----------
    subject_ids : np.array
        A NumPy array of the IDs of interest.
    category_name : str
        Name of column to plot against.
    id_name : str
        Name for group of ids submitted in ``subject_ids``.
    plot_title : str, optional
        Title to be assigned to plot. Blank by default.
    limit : int, optional
        Limit the amount of categories in the plot. By default will plot
        the counts of every category in the specified column.

    Returns
    -------
    ax : matplotlib Axes
        Returns the plot drawn onto the Axes.
    """
    # Get the counts of all category values for the inputted
    # ``subject_ids``
    subject_categories = (
        non_reversal_transactions[
            (non_reversal_transactions["transaction_id"].isin(subject_ids))
        ][category_name]
        .value_counts(normalize=True)[:limit]
        .reset_index(name="transaction_share")
        .rename(columns={"index": category_name})
    )
    subject_categories["transaction_type"] = id_name

    # Get the counts of all category values for all non-reversal
    # transactions
    all_transaction_categories = (
        non_reversal_transactions[category_name]
        .value_counts(normalize=True)[:limit]
        .reset_index(name="transaction_share")
        .rename(columns={"index": category_name})
    )
    all_transaction_categories["transaction_type"] = "All transactions"

    # Draw a bar plot comparing the two distributions
    ax = sns.barplot(
        data=pd.concat([subject_categories, all_transaction_categories]),
        y=category_name,
        x="transaction_share",
        hue="transaction_type",
    )
    ax.set_title(plot_title, loc="left")
    ax.legend(bbox_to_anchor=(1, 1), loc="upper left")
    ax.xaxis.set_major_formatter(mpl.ticker.StrMethodFormatter("{x:.0%}"))
    ax.axvline(0, c="#9E9E9E", zorder=1)
    ax.xaxis.grid(True)
    return ax


# %% {"Collapsed": "false"}
# Plot distribution comparison of merchantCategoryCode for reversals
plot_comparision_bar(
    reversal_transaction_ids,
    id_name="Reversed transactions",
    category_name="merchantCategoryCode",
    plot_title="Reversed transaction categories",
);

# %% {"Collapsed": "false"}
# Plot distribution comparison of merchantCategoryCode for multi-swipe
plot_comparision_bar(
    multi_swipe_transaction_ids,
    id_name="Multi-swipe transactions",
    category_name="merchantCategoryCode",
    plot_title="Multi-swipe transaction categories",
);

# %% [markdown] {"Collapsed": "false"}
# Estimated shares are displayed in the table below.

# %% {"Collapsed": "false"}
# Get the total transaction amount and number of transactions
# Exclude reversals
total_transaction_balance = non_reversal_transactions["transactionAmount"].sum()
number_of_transactions = non_reversal_transactions.shape[0]

# %% {"Collapsed": "false"}
# Return the number of reversals that are multi-swipes
np.intersect1d(
    reversal_transaction_ids.values, multi_swipe_transaction_ids.values
).shape[0]

# %% [markdown] {"Collapsed": "false"}
# It is natural to assume that many multi-swipes are eventually reversed. The code above shows that there is some overlap between reversal transactions and multi-swipe transactions. Therefore, we will count the total number of transactions and balances for the combined totals separately—as to not double count.

# %% {"Collapsed": "false"}
# Get totals for both multi-swipe and reversals
multi_swipe_and_reversal_transactions = transactions[
    transactions["transaction_id"].isin(reversal_transaction_ids)
    | transactions["transaction_id"].isin(multi_swipe_transaction_ids.values)
]
total_combined_balance = multi_swipe_and_reversal_transactions[
    "transactionAmount"
].sum()
total_combined = multi_swipe_and_reversal_transactions.shape[0]

# %% {"Collapsed": "false"}
# Create table
all_total_balances = [
    total_multi_swipe_balance,
    total_reversal_balance,
    total_combined_balance,
]
all_total_counts = [total_multi_swipes, total_reversals, total_combined]
overlap_report = pd.DataFrame(
    data={
        "total_balance": all_total_balances,
        "number_of_transactions": all_total_counts,
        "total_balance_proportion": [
            balance / total_transaction_balance for balance in all_total_balances
        ],
        "number_of_transactions_proportion": [
            count / number_of_transactions for count in all_total_counts
        ],
    },
    index=["multi_swipes", "reversals", "combined"],
)
overlap_report

# %% [markdown] {"Collapsed": "false"}
# ## Creating a model to detect fraud

# %% [markdown] {"Collapsed": "false"}
# One assumption I will be making is that fraud may be caught retroactively. I will be including features that indicate if the transaction was eventually reversed or if it is a multi-swipe. Both—especially the reversal—may happen some time after the initial charge occurs.

# %% [markdown] {"Collapsed": "false"}
# ### Clean data

# %% [markdown] {"Collapsed": "false"}
# In order to remove duplicate transactions, I will be excluding all reversal transactions and removing the multi-swipe transactions identified earlier. In both cases I will be retaining the original transactions.

# %% {"Collapsed": "false"}
# Make dataset without reversal or multi-swipe transactions
clean_transactions = non_reversal_transactions[
    ~non_reversal_transactions["transaction_id"].isin(multi_swipe_transaction_ids)
]

# Remove empty columns
clean_transactions = clean_transactions.drop(columns=empty_columns)

# %% {"Collapsed": "false"}
# Export for later use
clean_transactions.to_csv(
    pathlib.Path("data/cleaned/clean_transactions.csv"), index=False
)

# %% [markdown] {"Collapsed": "false"}
# ### Engineered features

# %% [markdown] {"Collapsed": "false"}
# Engineered features are listed as individual functions in the file [`features.py`](features.py). The function `create_features()` runs all the feature functions and outputs them as CSV files.
#
# The features engineered are:
#
# - Is the transaction a multi-swipe?
# - Is the transaction reversed?
# - Was the "cardCVV" equal to the "enteredCVV"?
# - Seconds since account was last involved in a transaction
# - Day of the week the transaction occurred
# - Month the transaction occurs
# - If the transaction occurs on a US holiday
# - If acqCountry equals merchantCountryCode
# - Seconds since address was changed
# - If transaction has a "special" merchantCategoryCode
#     - Special categories are: `fuel`, `mobileapps`, `online_subscriptions`, `food_delivery`, `gym`, and `cable/phone`
# - availableMoney divided by creditLimit
# - transactionAmount column divided by availableMoney
# - transactionAmount column divided by creditLimit
# - currentBalance column divided by creditLimit
# - transactionAmount column divided by availableMoney
# - Seconds until expiration
# - Seconds until account opens
# - If transaction is the account's first
# - If acqCountry is equal to that of the account's previous transaction
# - If merchantCountryCode is equal to the account's previous transaction.
#

# %% {"Collapsed": "false"}
# Read in data
clean_transactions = pd.read_csv(
    "data/cleaned/clean_transactions.csv",
    parse_dates=[
        "transactionDateTime",
        "currentExpDate",
        "accountOpenDate",
        "dateOfLastAddressChange",
    ],
)

# %% {"Collapsed": "false"}
# Create all features listed in ``features.py``
features.create_features(clean_transactions)


# %% {"Collapsed": "false"}
def fetch_features(
    directory: Union[str, pathlib.Path], data: pd.DataFrame, merge_column: str
) -> pd.DataFrame:
    """
    Merge in features stored in CSV in a directory.

    Assumes files all have one common column name to merge on, and that
    each file has no duplicate values in that column.

    Parameters
    ----------
    directory : Union[str, pathlib.Path]
        The directory where the feature CSV files are stored.
    data : pd.DataFrame
        The data which the features will be merged into.
    merge_column : str
        The name of the column to merge on. The DataFrame and each file
        must contain this column.

    Returns
    -------
    data_features : pd.DataFrame
        The original data with all features merged in.
    """
    data = data.copy()
    original_number_of_rows = data.shape[0]
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".csv"):
            print(filename)
            feature = pd.read_csv(os.path.join(directory, filename))
            data = data.merge(feature, on=merge_column, how="left")
            if data.shape[0] != original_number_of_rows:
                raise ValueError(
                    "A CSV file in the listed directory has duplicate values for"
                    " the column listed in merge_column"
                )
            if "Unnamed" in data.columns:
                warnings.warn("A column is unnamed")
    return data


# %% {"Collapsed": "false"}
transaction_features = fetch_features(
    pathlib.Path("data/features/"),
    data=clean_transactions,
    merge_column="transaction_id",
)

# %% [markdown] {"Collapsed": "false"}
# ### Missing values/pipeline

# %% [markdown] {"Collapsed": "false"}
# Of the remaining columns, five have missing values (not counting "seconds_since_account_transaction"—a generated feature).
#
# The following are my strategies for dealing with the missing values per column:
#
# - **merchantCountryCode** is already overwhelmingly "US", so missing values will be filled in with "US".
# - **acqCountry** is found to usually be the same as the country found in "merchantCountryCode". If
#     **merchantCountryCode** is available, use it. The class `AcqCountryImputer` was written to handle this case.
# - **transactionType** is highly correlated to "transactionAmount". For all recorded cases, if the "transactionType" is "ADDRESS_VERIFICATION", then the "transactionAmount" is found to be 0.00. So if the "transactionAmount" value is 0.00, then it is assumed the "transactionType" to be "ADDRESS_VERIFICATION". If not, it will be set to "PURCHASE". The class `TransactionTypeImputer` was written to handle this case.
# - **posEntryMode** and **posConditionCode** will each be filled with their most popular codes
#
# Once all missing values are filled, categorical values are one hot encoded.

# %% [markdown] {"Collapsed": "false"}
# As seen below, 99% of "merchantCountryCode" values are "US". Missing values in the column will be automatically filled with "US"

# %% {"Collapsed": "false"}
# Look at distribution of countries in "merchantCountryCode"
transaction_features["merchantCountryCode"].value_counts(normalize=True).reset_index(
    name="proportion"
)

# %% [markdown] {"Collapsed": "false"}
# The table above shows that most "acqCountry" values match their corresponding "merchantCountryCode". When available, missing "acqCountry" values will be set equal to "merchantCountryCode" values.

# %% {"Collapsed": "false"}
# Look at counts of pairs of countries involved in a transaction
transaction_features.groupby(["acqCountry", "merchantCountryCode"])[
    "transaction_id"
].count().reset_index(name="number_of_transactions")


# %% {"Collapsed": "false"}
# Create imputer for "acqCountry"
class AcqCountryImputer(BaseEstimator, TransformerMixin):
    """
    Fill in missing acqCountry values with merchantCountryCode value.

    If merchantCountryCode is also missing, fill in with most frequent
    value.
    """

    def __init__(self):
        self.most_frequent_country = None

    def fit(self, X: pd.DataFrame, y=None):
        """
        Record the most frequent value in the acqCountry column.

        Returns
        -------
        self : AcqCountryImputer
        """
        self.most_frequent_country = X["acqCountry"].mode().loc[0]
        return self

    def transform(self, X: pd.DataFrame, y=None) -> np.ndarray:
        """
        Impute all missing acqCountry values on X.

        Returns
        -------
        X : np.ndarray
            Data with missing acqCountry values imputed.
        """
        X = X.copy()
        X.loc[
            (~X["merchantCountryCode"].isna()) & (X["acqCountry"].isna()), "acqCountry"
        ] = X[(~X["merchantCountryCode"].isna()) & (X["acqCountry"].isna())][
            "merchantCountryCode"
        ]

        # For all remaining missing values, fill in with most frequent
        # value
        X["acqCountry"] = X["acqCountry"].fillna(self.most_frequent_country)
        return X[["acqCountry"]].values


# %% [markdown] {"Collapsed": "false"}
# All transactions with a "transactionType" value of "ADDRESS_VERIFICATION" have a "transactionAmount" of $0.00. If there is no "transactionAmount", then the "transactionType" is assumed to be "ADDRESS_VERIFICATION", else it will be assumed to be "PURCHASE".

# %% {"Collapsed": "false"}
# Find the total transaction amount for address verification transaction types
transaction_features[transaction_features["transactionType"] == "ADDRESS_VERIFICATION"][
    "transactionAmount"
].sum()


# %% {"Collapsed": "false"}
# Create imputer for "transactionType"
class TransactionTypeImputer(BaseEstimator, TransformerMixin):
    """
    Fill in missing transactionType values using transactionAmount.

    If transactionAmount is 0, fill in missing transactionAmount with
    "ADDRESS_VERIFICATION". Else fill with "PURCHASE".
    """

    def fit(self, X: pd.DataFrame, y=None):
        """Pass the transformer through."""
        return self

    def transform(self, X: pd.DataFrame, y=None) -> np.ndarray:
        """
        Impute all missing transactionType values on X.

        Returns
        -------
        X : np.ndarray
            Data with missing transactionType values imputed.
        """
        X = X.copy()
        X.loc[
            (["transactionAmount"] == 0) & (X["transactionType"].isna()),
            "transactionType",
        ] = "ADDRESS_VERIFICATION"

        # For all remaining missing values, fill in with "PURCHASE"
        X["transactionType"] = X["transactionType"].fillna("PURCHASE")
        return X[["transactionType"]].values


# %% {"Collapsed": "false"}
# Create pipeline applying different imputation strategies to
# their respective columns
impute_missing_categorical_pipe = ColumnTransformer(
    [
        (
            "frequent_imputer",
            SimpleImputer(strategy="most_frequent"),
            ["merchantCountryCode"],
        ),
        (
            "copy_country_imputer",
            AcqCountryImputer(),
            ["acqCountry", "merchantCountryCode"],
        ),
        (
            "pos_imputer",
            SimpleImputer(strategy="most_frequent"),
            ["posEntryMode", "posConditionCode"],
        ),
        (
            "transaction_type_imputer",
            TransactionTypeImputer(),
            ["transactionType", "transactionAmount"],
        ),
    ]
)

# %% {"Collapsed": "false"}
# Make a list of lists containing the categories values to be one hot encoded
missing_cat_onehot_categories = [
    np.sort(transaction_features[column].dropna().unique()).tolist()
    for column in (
        "merchantCountryCode",
        "acqCountry",
        "posEntryMode",
        "posConditionCode",
        "transactionType",
    )
]

# One hot encode after imputing missing values
missing_categorical_pipe = Pipeline(
    [
        ("impute_missing", impute_missing_categorical_pipe),
        ("onehot_missing", OneHotEncoder(categories=missing_cat_onehot_categories)),
    ]
)

# A list of the names of the columns that will be outputted by the one hot
# encoder
missing_cat_output_names = [
    f"{column}_{onehot_cat}"
    for i, column in enumerate(
        (
            "merchantCountryCode",
            "acqCountry",
            "posEntryMode",
            "posConditionCode",
            "transactionType",
        )
    )
    for onehot_cat in missing_cat_onehot_categories[i]
]

# %% {"Collapsed": "false"}
# Categorize features into different categories for pipeline
to_drop_columns = [
    "accountNumber",
    "customerId",
    "transactionDateTime",
    "currentExpDate",
    "dateOfLastAddressChange",
    "cardCVV",
    "enteredCVV",
    "cardLast4Digits",
    "transaction_id",
    "accountOpenDate",
    "isFraud",
]
missing_categorical_columns = [
    "merchantCountryCode",
    "acqCountry",
    "posEntryMode",
    "posConditionCode",
    "transactionType",
]
missing_continuous_columns = ["seconds_since_account_transaction"]
continuous_columns = [
    "availableMoney",
    "transactionAmount",
    "currentBalance",
    "seconds_until_expiration",
    "seconds_since_address_change",
    "currentBalance_over_creditLimit",
    "transactionAmount_over_availableMoney",
    "availableMoney_over_creditLimit",
    "seconds_since_account_opened",
    "transactionAmount_over_creditLimit",
    "currentBalanceover_availableMoney",
]
boolean_columns = [
    "cardPresent",
    "expirationDateKeyInMatch",
    "is_same_merchantCountryCode_as_prior",
    "is_first_account_transaction",
    "is_reversed",
    "is_same_acqCountry_as_prior",
    "is_special_merchantCategoryCode",
    "transaction_on_us_holiday",
    "is_multi_swipe",
    "is_cross_country_transaction",
    "is_correct_cvv",
]
categorial_columns = [
    "merchantName",
    "merchantCategoryCode",
    "transaction_month",
    "transaction_day_of_week",
]
ordinal_columns = ["creditLimit"]

# %% {"Collapsed": "false"}
# List out all categorical values for onehot encoding
onehot_categories = [
    np.sort(transaction_features[column].unique()).tolist()
    for column in categorial_columns
]

# Pipeline for imputing missing values and encoding categorical values
data_prep_pipe = ColumnTransformer(
    [
        ("dropped", "drop", to_drop_columns),
        (
            "missing_categorical",
            missing_categorical_pipe,
            missing_categorical_columns + ["transactionAmount"],
        ),
        (
            "missing_continuous",
            SimpleImputer(strategy="constant", fill_value=0),
            missing_continuous_columns,
        ),
        ("onehot", OneHotEncoder(categories=onehot_categories), categorial_columns),
        ("pass", "passthrough", continuous_columns + boolean_columns + ordinal_columns),
        (
            "missing_indicators",
            MissingIndicator(features="all"),
            missing_categorical_columns,
        ),
    ],
    n_jobs=-1,
)

# Record output names from "onehot" step in pipeline
onehot_output_names = [
    f"{column}_{onehot_cat}"
    for i, column in enumerate(categorial_columns)
    for onehot_cat in onehot_categories[i]
]

# Record output names for "missing_indicators" step in pipeline
missing_output_names = [f"missing_{column}" for column in missing_categorical_columns]

# List all columns outputted from onehot_categories pipeline
output_columns = (
    missing_cat_output_names
    + missing_continuous_columns
    + onehot_output_names
    + (continuous_columns + boolean_columns + ordinal_columns)
    + missing_output_names
)

# %% [markdown] {"Collapsed": "false"}
# ### Train model

# %% [markdown] {"Collapsed": "false"}
# The target value—"isFraud"—is very unbalanced, only 1.8% of all transactions were found to be fraud. To accommodate for this, the fraud cases were randomly oversampled to increase their representation in the training data.
#
# 80% of the total data was selected as a train set.
#
# Gradient boosted trees were selected as the model—where trees are sequentially added to an ensemble, each one correcting its predecessor. Gradient boosted trees were chosen due primarily to the short timeframe of the project. Gradient boosted trees are a good all-around model that do not require scaling of features and can employ early stopping for easy hyperparameter tuning. In particular, the _XGBoost_ implementation of gradient boosted trees was used primarily for its training performance. _XGBoost_ can parallelize tree construction, has early stopping built in, and can bin continuous features.
#
# The average precision was used to evaluate the model and tune the number of trees. It was chosen because it is a metric that summarizes the precision-recall curve and is fairly robust to imbalanced classes—as is the case in this dataset.
#
# While not shown, isolation forests were also fit to the data—as they typically are proficient at outlier detection. However, at first glance this model seemed to underperform in comparison gradient boosted trees, so it was cast aside in favor of the current implementation.

# %% {"Collapsed": "false"}
# Look at distribution of ``isFraud``
clean_transactions["isFraud"].value_counts(normalize=True).reset_index(
    name="proportion"
).rename(columns={"index": "isFraud"})

# %% {"Collapsed": "false"}
# Use a stratified split to get train and test set of data
X_train, X_test, y_train, y_test = train_test_split(
    transaction_features,
    transaction_features[["isFraud"]],
    test_size=0.2,
    random_state=42,
    stratify=transaction_features[["isFraud"]],
)

# %% {"Collapsed": "false"}
# Employ early stopping for easy hyperparameter tuning

# Using only the train data, make a stratified test train split for use
# in early stopping
X_early_train, X_early_test, y_early_train, y_early_test = train_test_split(
    X_train, y_train, test_size=0.2, random_state=42, stratify=y_train
)

# Fit pipeline on early stopping train set
X_early_train = data_prep_pipe.fit_transform(X_early_train)

# Oversample early stopping train set
oversample = RandomOverSampler(random_state=42, sampling_strategy="minority")
X_early_train, y_early_train = oversample.fit_resample(
    X_early_train, y_early_train["isFraud"]
)

# Execute early stopping
# For speed, bin the continuous values
clf_early = XGBClassifier(
    n_jobs=-1, tree_method="hist", random_state=42, n_estimators=5_000
)
X_early_test = data_prep_pipe.transform(X_early_test)
clf_early.fit(
    X_early_train,
    y_early_train,
    early_stopping_rounds=50,
    eval_set=[(X_early_test, y_early_test["isFraud"])],
    verbose=True,
    eval_metric="map",
)

# %% [markdown] {"Collapsed": "false"}
# Best score is 581 trees (0.091).

# %% {"Collapsed": "false"}
y_train["isFraud"].value_counts(normalize=True)

# %% {"Collapsed": "false"}
# Full modeling pipeline
full_pipe = ImPipeline(
    [
        # The data prep pipeline from the previous step
        ("data_prep", data_prep_pipe),
        # Overample the isFraud = 1 cases
        (
            "oversample",
            RandomOverSampler(random_state=42, sampling_strategy="minority"),
        ),
        # Fit an ensemble of gradient boosted trees with the number of
        # estimators determined by the early stopping
        (
            "clf",
            XGBClassifier(
                n_jobs=-1, tree_method="hist", random_state=42, n_estimators=581
            ),
        ),
    ]
)

# %% {"Collapsed": "false"}
# A full modeling attempt would include proper hyperparameter tuning via
# Grid/Randomized Search or hyperopt
# from sklearn.model_selection import RandomizedSearchCV

# param_grid = {
#     "clf__learning_rate": [0.05, 0.10, 0.15, 0.20, 0.25, 0.30],
#     "clf__max_depth": [3, 4, 5, 6, 8, 10, 12, 15],
#     "clf__min_child_weight": [1, 3, 5, 7],
#     "clf__gamma": [0.0, 0.1, 0.2, 0.3, 0.4],
#     "clf__colsample_bytree": [0.3, 0.4, 0.5, 0.7],
# }

# RandomizedSearchCV(
#     full_pipe,
#     param_grid=param_grid,
#     scoring="average_precision",
#     cv=5,
#     verbose=10,
#     random_state=42,
#     n_iter=1_000,
#     return_train_score=True,
# )

# %% [markdown] {"Collapsed": "false"}
# ### Model performance

# %% [markdown] {"Collapsed": "false"}
# The model was evaluated using 3-fold cross validation. The mean of all average precision scores was found to be `0.084` . For context, the baseline average precision of a random classifier for this dataset is `0.018`. The model is a 467% improvement over a random classifier.
#
# Accompanying plots are included to illustrate the model's performance.

# %% {"Collapsed": "false"}
# Score model using cross validation
cross_val_avg_precision = cross_val_score(
    full_pipe,
    X=X_train,
    y=y_train["isFraud"],
    n_jobs=-1,
    cv=3,
    verbose=10,
    scoring="average_precision",
)
cross_val_avg_precision
np.mean(cross_val_avg_precision)

# %% {"Collapsed": "false"}
# Obtain the model's predictions on the training set to visualize its
# performance
predict_proba = cross_val_predict(
    full_pipe,
    X=X_train,
    y=y_train["isFraud"],
    n_jobs=-1,
    cv=3,
    verbose=10,
    method="predict_proba",
)

# Convert the probabilities from predict_proba to binary values
predict_vals = np.argmax(predict_proba, axis=1)

# %% [markdown] {"Collapsed": "false"}
# The precision-recall curve is related to the average precision—the metric the model aimed to maximize. The curve—along with the accompanying ROC curve—illustrate the tradeoffs of different decision threshold values of the model.

# %% {"Collapsed": "false"}
model_eval_plots.plot_pr(
    y_train["isFraud"], predict_proba, title="Precision-recall curve"
)

# %% {"Collapsed": "false"}
model_eval_plots.plot_roc(y_train["isFraud"], predict_proba, title="ROC curve")

# %% [markdown] {"Collapsed": "false"}
# The prediction probability distribution plot shows kernel density estimation of the model's predicted probabilities for transactions and colors them according to whether the transaction was fraudulent. Encouragingly, there is a clear distinction in the distribution of the predicted probabilities for the two classes, which means the model is effectively picking out signals that imply a fraudulent transaction. However, the two distributions have a large overlap, which means there is room to improve to help the model further separate the two classes. Interestingly, there is a sharp peak on the far left, indicating a group of transactions that the model is very sure are not fraudulent. It is worth clarifying that the areas under both curves are normalized to be equal, so even though fraudulent transactions are a fraction of the number of legitimate transactions, both peaks stand at about equal height.
#
# The accompanying confusion matrix highlights that the model has a high amount of false positives, and could use some further work to help improve its precision.

# %% {"Collapsed": "false"}
model_eval_plots.plot_probability_dist(
    y_train["isFraud"], predict_proba, title="Prediction distribution"
)

# %% {"Collapsed": "false"}
model_eval_plots.plot_confusion(
    y_train["isFraud"], predict_proba, title="Confusion matrix"
);

# %% [markdown] {"Collapsed": "false"}
# ### Model analysis

# %% [markdown] {"Collapsed": "false"}
# To provide some intuition for the model, and inform what next steps should be taken to further improve its performance, Shapley values are calculated for the predictions.
#
# The Shapley values reveal that `is_special_merchantCategoryCode` is highly influential feature. It was an engineered feature that indicated whether the transaction involved a fuel, mobileapps, online_subscription, food_delivery, gym, or cable/phone merchant. These categories were deemed unique because no multi-swipe or reversed transactions were detected in those transactions.
#
# Additionally, it can be seen that the data lacks strong indicators of a fraudulent transaction. Only five of the top 20 features are shown to contribute positive Shapley values (meaning they push the prediction towards identifying the transaction as fraudulent). The `transactionAmount` was the biggest contributor—with high values making it more likely that the transaction is fraudulent.
#
# An accompanying plot looks at the Shapley values just for `transactionAmount` reveals an interesting relationship with the feature `creditLimit`. When the `transactionAmount` is low the `creditLimit` being high will mean a lower chance of the model predicting the transaction to be fraudulent, while if the `transactionAmount` is high, a high `creditLimit` will result in the model predicting a higher chance of predicting fraud. The feature `creditLimit` interacts similarly with other features—as shown in other plots
#
# The final feature to be highlighted is the Shapley values for the false negatives—the fraud the model missed out on. It can be seen that the `transactionAmount` for these false negatives is typically low. The model relies heavily on higher `transactionAmount` values for fraud detection, and needs improvement when dealing with lower transaction amounts.

# %% {"Collapsed": "false"}
# Train model for shap analysis since shap is not compatible with
# scikit-learn pipelines

# Using only the train data, make a stratified test train split for use
# in SHAP analysis
X_shap_train, X_shap_test, y_shap_train, y_shap_test = train_test_split(
    X_train, y_train, test_size=0.2, random_state=42, stratify=y_train
)

# Fit pipeline on SHAP train set
X_shap_train = data_prep_pipe.fit_transform(X_shap_train)
X_shap_test = data_prep_pipe.transform(X_shap_test)

# Oversample SHAP train
oversample = RandomOverSampler(random_state=42, sampling_strategy="minority")
X_shap_train, y_shap_train = oversample.fit_resample(
    X_shap_train, y_shap_train["isFraud"]
)

# Train model for shap analysis
clf_shap = XGBClassifier(
    n_jobs=-1, tree_method="hist", random_state=42, n_estimators=581
)
clf_shap.fit(X_shap_train, y_shap_train)

# %% {"Collapsed": "false"}
# Show SHAP plot
X_shap_test_df = pd.DataFrame(
    X_shap_test.todense(), columns=output_columns, index=[i for i in range(99130)]
)
explainer = shap.TreeExplainer(clf_shap)
shap_values = explainer.shap_values(X_shap_test_df)
shap.summary_plot(shap_values, X_shap_test_df)

# %% {"Collapsed": "false"}
# Look at shap values for transactionAmount and it's relationship to
# creditLimit
shap.dependence_plot("transactionAmount", shap_values, X_shap_test_df)

# %% {"Collapsed": "false"}
# Look at shap values for seconds_since_account_transaction and it's
# relationship to creditLimit
shap.dependence_plot("seconds_since_account_transaction", shap_values, X_shap_test_df)

# %% {"Collapsed": "false"}
# Look at shap values for seconds_until_expiration and it's relationship
# to creditLimit
shap.dependence_plot("seconds_until_expiration", shap_values, X_shap_test_df)

# %% {"Collapsed": "false"}
# Output probabilities for model for shap analysis
y_predict_proba_shap = clf_shap.predict_proba(X_shap_test)

# %% {"Collapsed": "false"}
# Shap values for false negatives
false_negatives_filer = (
    (y_shap_test["isFraud"] == 1) & (y_predict_proba_shap[:, 1] < 0.5)
).values
shap.summary_plot(
    shap_values[false_negatives_filer], X_shap_test_df[false_negatives_filer]
)

# %% {"Collapsed": "false"}
# Look at shap values for transactionAmount and it's relationship to
# seconds_since_account_transaction for false negative transactions
shap.dependence_plot(
    "transactionAmount",
    shap_values[false_negatives_filer],
    X_shap_test_df[false_negatives_filer],
    title="False negatives",
)

# %% {"Collapsed": "false"}
# Shap values for false positives
false_positives_filer = (
    (y_shap_test["isFraud"] == 0) & (y_predict_proba_shap[:, 1] > 0.5)
).values
shap.summary_plot(
    shap_values[false_positives_filer], X_shap_test_df[false_positives_filer]
)

# %% {"Collapsed": "false"}
# Shap values for confident true negatives (probability of less than 5%)
false_negatives_filer = (
    (y_shap_test["isFraud"] == 0) & (y_predict_proba_shap[:, 1] < 0.05)
).values
shap.summary_plot(
    shap_values[false_negatives_filer], X_shap_test_df[false_negatives_filer]
)

# %% [markdown] {"Collapsed": "false"}
# ### Future work

# %% [markdown] {"Collapsed": "false"}
# The model is off to a great start, but needs some improvement in distinguishing fraudulent transactions.
#
# One obvious step I omitted is grid search to optimize hyperparameters. Similarly I would look at other models such as logistic regression, and other anomaly detection models such as Gaussian mixture models and compare their performance to the current gradient boosted tree implementation.
#
# However, much more importantly, big gains in model performance are gained through data cleaning and feature engineering. Given the time and more computing power, I would engineer more features. Some of the features I would like to engineer are:
#
# - Number of transactions in the last hour/day/week/all-time for account/customer
# - Total transaction amount in the last hour/day/week/all-time for account/customer
# - Most popular merchant category/name of account/customer
#   - Does the transaction involve the most popular merchant category/name?
# - Is the transaction at a new merchant category/name for the account/customer?
# - Has the customer/account been a victim of fraud before?
#   - Total amount of fraud incidents for account/customer
# - Average number of transactions per day/week for customer/account
# - Average transaction amount per day/week for customer/account
# - Holidays for different countries (only used US federal holidays)
#
# When filling out the missing values for the columns "posEntryMode" and "posConditionCode" I simply filled them in with their most popular values. This is rarely the optimal method. If given more time I would search the data for a relationship with other values as done with other columns, find the most popular values for cohorts of transactions (the most popular "posEntryMode" per "transactionAmount" bin, etc.), or use a k-nearest neighbors model to more intelligently fill in missing values. I had lookup up some basic definitions for the two columns—"posEntryMode" identifies the method used to capture the account number, and "posConditionCode" identifies transaction conditions—but confidently knowing exactly what each code stood for would help in both filling values, feature engineering, and interpreting model Shapley values.
#
# Additionally, when identifying multi-swipe and reversed transactions, I semi-arbitrarily chose 12 hours as the time window for seeking out these duplicate transactions. I know this led to some possible duplicate transactions being cut out. I would like to consult with a subject matter expect to tune this window appropriately.
#
